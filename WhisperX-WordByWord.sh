#!/bin/bash

fix_time () {
number=$1
time=$2
fix=false
clear=false
N=1
while IFS= read -r line; do
    if [[ $fix == true ]] then
        line="${line:0:17}$time"
        fix=false
        echo "$line" >> .1
    fi
    if [[ $clear == true ]] then
        line='N'$N
        echo "$line" >> .1
        return
    fi
    if [[ $line == $(($number-1)) ]] then
        fix=true
    fi
    if [[ $line == $number ]] then
        line='N'$N
        clear=true
        echo "$line" >> .1
    fi
    
    N=$(($N+1))
done < .0
}


#script start
if ! [[ $1 == *.srt ]] then
 echo "ERROR: no .srt file specified"
 echo
 echo "This script works with .srt files created by WhisperX with the --highlight_words perameter enabled." 
 echo "Using .srt files from other soruces will result in an empty output file, and my cause unwanted behavioir."
 echo
 echo "USAGE: ./WhisperX-WordByWord.sh myfile.srt"
 echo
 exit
fi

#entry into main loop
for file in $1; do
echo "file: "$file

#first pass, isolate words
echo "isolating words..."
while IFS= read -r line; do
    line=${line#*<u>}
    line=${line%%</u>*}
    
    echo "$line" >> .0
done < $file


#scocond pass, generate new times
echo "adjusting timestamps..."
re='^[0-9]+$'
I=1
while IFS= read -r line; do
    #get timing variables
    test=${line:0:3}
    if [[ $test =~ $re ]] then
        number=$line
    fi
    test=${line:13}
    if [[ ${test:0:3} == "-->" ]] then
        time=${test:4}
    fi 
    
    #clear lines
    test=${line:0:1}
    if ! [[ $test =~ $re ]] then
        if ! [[ $(expr length "$line") =  $(expr length "${line#* }") ]] then
            echo 'N'$I >> .1
            fix_time "$number" "$time"
        fi
    fi
    I=$(($I+1))
done < .0


#third pass, fix times and remove leftover titles
echo "generating new file..."
N=1
while IFS= read -r line; do
    while IFS= read -r time; do
        if [[ ${line:0:12} == ${time:0:12} ]] then
            line=$time
            break
        elif [[ ${time:0:1} == 'N' && ${time#*N} == $N ]] then
            line=''
            break
        fi
    done < .1
    
    echo $line >> ${file%%.srt}-wbw.srt
    N=$(($N+1))
done < .0

#rm .1
#rm .0
echo "Done!"
echo""
done
